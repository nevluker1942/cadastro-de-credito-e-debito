<!DOCTYPE html>
<html lang="en">
<form method="POST" enctype="multipart/form-data" action="funcao.php">

<?php include("head.php") ?>
<?php include("conn.php") ?>
<?php include("funcao.php") ?>

<body id="page-top">

<nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
    <a class="navbar-brand js-scroll-trigger" href="#page-top">
      <span class="d-block d-lg-none">Gerenciador de Gastos Pessoais</span>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#about">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#experience">Extrato</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#education">Débitos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#skills">Créditos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#interests">Sair</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="container-fluid p-0">

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="about">
      <div class="w-100">
        <h1 class="mb-0">Gerenciador de 
          <span class="text-primary">Gastos Pessoais</span>
        </h1>
        <div class="subheading mb-5">&nbsp Sistema desenvolvido para controlar a movimentação de sua renda.
        </div>
        <h3 class="text-primary">&nbsp Saldo Atual :<?php '$saldoatual' ?> </h3  >
      </div>
    </section>

    <hr class="m-0">

    <!-- EXTRATO -->
    <script type="text/javascript" src="ajax.js"></script>
    <section class="resume-section p-3 p-lg-5 d-flex justify-content-center" id="experience">
      <div class="w-100">
        <h2 class="mb-5">Extrato</h2>
        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Selecione o período desejado: </h3>
            <br><br>
            <table cellpading="10">
              <tr>
                <td>
                  <input type="date" name="datainicio" id="datain">
                </td>
                <td>
                  &nbsp<strong>à<strong>&nbsp <input type="date" name="datafim" id="dataf">
                </td>
                <td>
                  <input type="button" value="Consultar" name="btextrato" onclick="getDados();">
                </td>  
              </tr>   
            </table> 
            <br><br>

            <div id="Resultado"></div>
        </div>

        

        

      </div>

    </section>

    


    <hr class="m-0">

    <!-- DÉBITOS -->

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="education">
      <div class="w-100">
        <h2 class="mb-5">DÉBITOS</h2>

        <table cellpadding="10">
        <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
          <div class="resume-content">
            <h3 class="mb-0">Adicionar Débitos</h3>
            <tr>
            <td align="right">
              <div class="subheading mb-3">
                Descrição do Débito:
            </td>
            <td>
              <input type="text" name="descricaodebito" placeholder="Exemplo: Luz">
            </td>
            </tr>
            <tr>
            <td align="right"> 
              </div>
              <div class="subheading mb-3">
                Valor do Débito:
            </td>
            <td>
                <input type="text" name="valordebito" placeholder="Exemplo: 255.00">
            </td>
            </tr>
            <tr>
            <td align="right">
             </div>
            <div class="subheading mb-3">
              Data do Débito:
            </td>
            <td>
              <input type="date" name="datadebito">
            </td>
          </tr>
            </div>
        </div>
        <tr>
          <td colspan="2" align="center">
            <input type="submit" name="btenviadebito" value="Cadastrar">
          </td>
        </tr>
        </table>

      </div>
    </section>

    <hr class="m-0">

    <!-- CRÉDITOS -->

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="skills">
      <div class="w-100">
        <h2 class="mb-5">CRÉDITOS</h2>
        <table cellpadding="10">
            <div class="resume-item d-flex flex-column flex-md-row justify-content-between mb-5">
              <div class="resume-content">
                <h3 class="mb-0">Adicionar Créditos</h3>
                <tr>
                <td align="right">
                  <div class="subheading mb-3">
                    Descrição do Crédito:
                </td>
                <td>
                  <input type="text" name="descricaoCredito" placeholder="Exemplo: Salário">
                </td>
                </tr>
                <tr>
                <td align="right"> 
                  </div>
                  <div class="subheading mb-3">
                    Valor do Crédito:
                </td>
                <td>
                    <input type="text" name="valorCredito" placeholder="Exemplo: 1000.00">
                </td>
                </tr>
                <tr>
                <td align="right">
                 </div>
                <div class="subheading mb-3">
                  Data do Crédito:
                </td>
                <td>
                  <input type="date" name="dataCredito">
                </td>
              </tr>
                </div>
            </div>
            <tr>
              <td colspan="2" align="center">
                <input type="submit" name="btenviacredito" value="Cadastrar">
              </td>
            </tr>
            </table>
      </div>
    </section>

    <hr class="m-0">

    <!-- SAIR -->

    <section class="resume-section p-3 p-lg-5 d-flex align-items-center" id="interests">
      <div class="w-100">
        <h2 class="mb-5">Interests</h2>
        <p>Apart from being a web developer, I enjoy most of my time being outdoors. In the winter, I am an avid skier and novice ice climber. During the warmer months here in Colorado, I enjoy mountain biking, free climbing, and kayaking.</p>
        <p class="mb-0">When forced indoors, I follow a number of sci-fi and fantasy genre movies and television shows, I am an aspiring chef, and I spend a large amount of my free time exploring the latest technology advancements in the front-end web development world.</p>
      </div>
    </section>

    <hr class="m-0">


  </div>

  </form>

<?php include("jseboots.php") ?>

</body>

</html>
